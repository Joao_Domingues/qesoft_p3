package graphwalker;

import org.graphwalker.core.condition.EdgeCoverage;
import org.graphwalker.core.condition.TimeDuration;
import org.graphwalker.core.condition.VertexCoverage;
import org.graphwalker.core.generator.QuickRandomPath;
import org.graphwalker.core.generator.RandomPath;
import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Vertex;
import org.graphwalker.java.test.TestBuilder;
import org.junit.Test;

import controllers.TriangleController;
import controllers.UserController;
import domain.Triangle;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class TriangleControllerTest extends ExecutionContext implements GraphWalkerTest {
	public final static Path MODEL_PATH = Paths.get("graphwalker/testModel.json");

	private final UserController userController = new UserController();
	private final TriangleController triangleController = new TriangleController();

	
	public void eRetryLogin() {
		// TODO Auto-generated method stub
		
	}

	public void vLogin() {
		String login = "notValid";
		double random = Math.random() ;
		if(random > 0.3) login = "admin";	
		setAttribute("validLogin", userController.login(login, login));	
	}


	// Option Values
	// 1 - Add
	// 2 - Delete
	// 3 - Edit
	// 4 - GetAll
	// 5 - Logout
	// 6/7 - Error
	public void vMenu() {
		int max = 7;
		int min = 1;
		int random = (int) (Math.random() * (max - min) + min);
		
		setAttribute("option", random);
		
	}
	

	public void eMenu() {
		// TODO Auto-generated method stub
		
	}


	public void eErrorSelectToEdit() {
		// TODO Auto-generated method stub
		
	}


	public void eErrorEditTriangle() {
		// TODO Auto-generated method stub
		
	}


	public void vAddTriangle() {
		double random = Math.random() ;
		if(random>0.3) {
			triangleController.create(5, 5, 5);
			setAttribute("addTriangle", true);
		} else {
			setAttribute("addTriangle", false);
		}
		
	}

	public void eEditedTriangle() {
		// TODO Auto-generated method stub
		
	}


	public void vGetAll() {
		if(triangleController.getAll().size()==0) {
			setAttribute("getAll", false);
		} else {
			setAttribute("getAll", true);
		}
		
	}

	public void eDeleted() {
		// TODO Auto-generated method stub
		
	}

	public void eDelete() {
		// TODO Auto-generated method stub
		
	}

	public void eErrorGetAll() {
		// TODO Auto-generated method stub
		
	}

	public void vErrorDelete() {
		// TODO Auto-generated method stub
		
	}

	public void vEditTriangle() {
		double random = Math.random() ;
		if(random>0.3) {
			triangleController.update(Integer.valueOf(getAttribute("idToEdit").toString()), 6, 6, 6);
			setAttribute("editTriangle", true);
		} else {
			setAttribute("editTriangle", false);
		}
		
	}

	public void eAddedTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void eAddTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void eBackToMenuFrom() {
		// TODO Auto-generated method stub
		
	}

	public void eEditTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void eBackToMenuFromErrorEditTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void eRetryMenu() {
		// TODO Auto-generated method stub
		
	}

	public void eBackToMenuFromErrorGetAll() {
		// TODO Auto-generated method stub
		
	}

	public void vErrorTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void vErrorEditTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void vDelete() {

		int max = 5;
		int min = 0;
		int id = (int) (Math.random() * ((max - min)) + min);
		setAttribute("deleted", triangleController.delete(id));
		
	}

	public void vErrorSelectToEdit() {
		// TODO Auto-generated method stub
		
	}

	public void eGetAll() {
		// TODO Auto-generated method stub
		
	}

	public void eErrorDelete() {
		// TODO Auto-generated method stub
		
	}

	public void eWrongTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void eGotAll() {
		// TODO Auto-generated method stub
		
	}

	public void vErrorGetAll() {
		// TODO Auto-generated method stub
		
	}

	public void vSelectToEdit() {
		int max = 8;
		int min = 0;
		int id = (int) (Math.random() * ((max - min)) + min);
		if(triangleController.getById(id)==null) {
			setAttribute("selectToEdit", false);
		} else {
			setAttribute("selectToEdit", true);
			setAttribute("idToEdit", id);
		}
		
	}

	public void eBackToMenuFromErrorTriangle() {
		// TODO Auto-generated method stub
		
	}

	public void eSelectToEdit() {
		// TODO Auto-generated method stub
		
	}

	public void eBackToMenuFromErrorSelectToEdit() {
		// TODO Auto-generated method stub
		
	}
	
	public void eLogout() {
		// TODO Auto-generated method stub
		
	}

	
//	@Test
//    public void vertexCoverageTest() {
//        new TestBuilder()
//                .addContext(new TriangleControllerTest().setNextElement(new Vertex().setName("vLogin").build()),
//                        MODEL_PATH,
//                        new QuickRandomPath(new VertexCoverage(100)))
//                .execute();
//    }
	
	@Test
    public void edgeCoverageTest() {
        new TestBuilder()
                .addContext(new TriangleControllerTest().setNextElement(new Vertex().setName("vLogin").build()),
                        MODEL_PATH,
                        new QuickRandomPath(new EdgeCoverage(100)))
                .execute();
    }


}
