package controllers;

import domain.Triangle;
import persistence.*;

import java.util.List;
import java.util.Map;

public class TriangleController {
	
	private ShapeRepository repo;
	
	public TriangleController() {
		repo = ShapeRepository.getInstance();
	}
	
	public String create(int a, int b, int c) {
		Triangle t = new Triangle(a, b, c);
		if(t.isTriangle()) {
			repo.save(t);
			return "TA TOP";	
		} else {
			return "NAO TA TOP";
		}
	}
	
	public boolean delete(int id) {
		return repo.delete(id);
	}
	
	public Triangle read(int id) {
		return repo.get(id);
	}

	public boolean exists(int id){
		return repo.exists(id);
	}
	
	public Boolean update(int id, int a, int b, int c) {
		return repo.update(id, new Triangle(a, b, c));
	}
	
	public List<Triangle> getAll() {
		return repo.getAll();
	}
	
	public Triangle getById(int id) {
		return repo.getById(id);
	}
	
	
}
