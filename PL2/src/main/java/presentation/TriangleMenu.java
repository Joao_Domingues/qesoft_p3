package presentation;

import java.util.List;
import java.util.Scanner;

import controllers.TriangleController;
import domain.Triangle;
import persistence.ShapeRepository;

import java.util.Map;

public class TriangleMenu {

    private static Scanner read = new Scanner(System.in);
    private static TriangleController controller = new TriangleController();
    private static ShapeRepository rep = ShapeRepository.getInstance();

    public static void init() {
        int option;

        do {
            System.out.println("\n\n -- Choose an option: --");
            System.out.println("0 - Exit\n1 - Add new Triangle\n2 - Delete Triangle\n3 - Update Triangle\n4 - Show All Triangles");

            Scanner read = new Scanner(System.in);

            option = read.nextInt();
            switch (option) {
                case 1:
                    addTriangle();
                    break;
                case 2:
                    deleteTriangle();
                    break;
                case 3:
                    updateTriangle();
                    break;
                case 4:
                    showAll();
                    break;
                default:
                    System.out.println("Option does not exist!");
            }
        } while (option != 0);
		System.out.println("BYE...");
    }

    private static void addTriangle() {
        String auxC="";
        System.out.println("Insert 3 values:");

        System.out.println("Value 1:");
        int a = read.nextInt();
        System.out.println("Value 2:");
        int b = read.nextInt();
        System.out.println("Value 3:");
        int c = read.nextInt();

        if (a + b < c || a + c < b || b + c < a) {
            System.out.println("ERROR: INVALID - Press Any Key to continue..");
        } else {
            controller.create(a, b, c);
            System.out.println("Created - Press Any key to Continue");
            auxC= read.next();
        }
    }

    private static void deleteTriangle() {
        String auxC="";

        System.out.println("Insert id:");

        int id = read.nextInt();
        boolean res = controller.delete(id);
        if (res) {
            System.out.println("Success - Press Any Key to continue..");
            auxC=read.next();
        } else {
            System.out.println("ERROR: INVALID - Press Any Key to continue..");
        }
    }


    private static void updateTriangle() {
        String auxC="";
        System.out.println("Please insert the id of the triangle you pretend to update: ");
        int id = read.nextInt();
        Triangle t = controller.getById(id);
        if(t==null) {
        	 System.out.println("ERROR: INVALID - Press Any Key to continue..");
        } else {	        
	        System.out.println("Please insert 1st triangle side: ");
	        int a = read.nextInt();
	        System.out.println("Please insert 2nd triangle side: ");
	        int b = read.nextInt();
	        System.out.println("Please insert 3rd triangle side: ");
	        int c = read.nextInt();
            if (a + b < c || a + c < b || b + c < a) {
                System.out.println("ERROR: INVALID - Press Any Key to continue..");
            } else {
                if(controller.update(id, a, b, c)) {
                    System.out.println("Created - Press Any key to Continue");
                    auxC = read.next();
                }else{
                    System.out.println("ERROR: INVALID - Press Any Key to continue..");

                }
            }
        }
    }

    private static void showAll() {
        String auxC="";
        List<Triangle> map = controller.getAll();
        if(map.size()==0) {
        	System.out.println("There are currently no triangles available");
        } else {
	        for (Triangle entry : map) {
	            System.out.println("ID: " + entry.id + " : " + entry.toString());
	        }
        }
        System.out.println("Show complete - Press Any key to Continue");
        auxC=read.next();
    }
}
