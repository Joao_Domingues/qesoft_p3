package persistence;

import domain.Triangle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


public class ShapeRepository {

    private List<Triangle> map = new ArrayList();
    private int idCount = 0;

    private static ShapeRepository instance = null;

    public static ShapeRepository getInstance() {
        if (instance == null) instance = new ShapeRepository();
        return instance;
    }

    public void save(Triangle triangle) {
        triangle.setId(idCount++);
        map.add(triangle);
    }

    public boolean delete(int id) {

        boolean validate = false;
        int index = 0;
        for (Triangle t : this.map) {
            if (t.id == id) {
                validate = true;
                index = map.indexOf(t);
            }
        }
        if (validate) {
            map.remove(index);
            return true;
        }
        return false;
    }

    public Triangle get(int id) {
        return map.get(id);
    }

    public boolean exists(int id) {
        for (Triangle t : this.map) {
            if (t.id == id) {
                return true;
            }
        }
        return false;
    }


    public boolean update(int id, Triangle triangle) {

		for (Triangle t : this.map) {
			if (t.id == id) {
				t.sideB = triangle.sideB;
				t.sideA = triangle.sideA;
				t.sideC = triangle.sideC;
				return true;
			}
		}
		return false;
    }

    public List<Triangle> getAll() {
        return map;
    }
    
    public Triangle getById(int id) {
    	for (Triangle t : this.map) {
			if (t.id == id) {
				return t;
			}
		}
    	return null;
    }

}
