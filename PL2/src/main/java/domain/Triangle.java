package domain;

public class Triangle {
	public int id;
	public int sideA;
	public int sideB;
	public int sideC;
	
	public Triangle(int sideA, int sideB, int sideC) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.sideC = sideC;
	}

	public void setId(int id){
	    this.id=id;
    }
	public boolean isTriangle() {
        if(sideA + sideB < sideC) return false;
        if(sideA + sideC < sideB) return false;
        if(sideB + sideC < sideA) return false;
        return true;
    }

	public String type() {
        String type = "";

        if(sideA == sideB && sideB == sideC) type = "Equilateral";
        else if(sideA == sideB || sideB == sideC || sideA == sideC) type = "Isosceles";
        else type = "Scalene";

        return type;
    }
    
    public double getArea() {
    	double s = 0.5 * (sideA + sideB + sideC);
        double area = Math.sqrt(s*(s-sideA)*(s-sideB)*(s-sideC));
        System.out.println("The area of the triangle is " + area);
        return area;
    }
    
    @Override
    public String toString() {
    	return "Triangle with the sides: " + sideA + ", " + sideB + " & " + sideC;
    }
}
