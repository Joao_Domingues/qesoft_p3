package domain;

public class Rectangle {
	private int sideA;
	private int sideB;
	
	public Rectangle(int sideA, int sideB) {
		this.sideA = sideA;
		this.sideB = sideB;
	}
	
	public double getArea() {
        return sideA * sideB;
    }
}
