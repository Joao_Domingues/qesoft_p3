Feature: Triangle
  As a user
  I want to know which type of triangle I have
  Scenario: Submit three numbers 10, 10 & 15
    Given I have a triangle
    When I test 10, 10 & 15
    Then The result should be "Isosceles"
  Scenario: Submit three numbers 15, 15 & 15
    Given I have a triangle
    When I test 15, 15 & 15
    Then The result should be "Equilateral"
  Scenario: Submit three numbers 10, 15 & 20
    Given I have a triangle
    When I test 10, 15 & 20
    Then The result should be "Scalene"